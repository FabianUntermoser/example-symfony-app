<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExampleResponseController
{
    /**
     * @Route("/lucky/number")
     * @Method({"GET"})
     * @return Response
     * @throws \Exception
     */
    public function number(){
        $number = random_int(0,100);

        return new Response(
            '<html><body>Lucky number: ' . $number . '</body></html>'
        );
    }

}
