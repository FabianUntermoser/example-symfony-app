<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExampleTwigController extends Controller
{
    /**
     * @Route("/hello")
     * @Method({"GET"})
     * @return Response
     */
    function test()
    {
        $name = "Fabian";
        return $this->render('articles/hello.html.twig', array('name' => $name));
    }
}
