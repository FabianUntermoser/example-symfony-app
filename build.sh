#!/usr/bin/env bash

docker build . --tag my-symfony-app

docker container rm -f symfony-app
docker run --name symfony-app -d -p 8080:80 -v $(pwd):/var/www/html my-symfony-app

docker container rm -f symfony-db
docker run -d --name symfony-db -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234 mysql:latest

# creating database
docker exec -it symfony-app php bin/console doctrine:database:create

